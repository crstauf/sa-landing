<?php
header("Content-type: text/css; charset: UTF-8");

if (array_key_exists('ul-bullet',$_GET)) {
    $ul_bullet_image = $_GET['ul-bullet'];
	echo 'body.has-ul-bullet-image ul > li:before { width: ' . filter_var($ul_bullet_image[1]) . 'px; height: ' . filter_var($ul_bullet_image[2]) . 'px; background-image: url(' . filter_var($ul_bullet_image[0],FILTER_SANITIZE_URL) . '); }';
}

?>
