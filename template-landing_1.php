<?php
/* Template name: Landing #1 */

if (!defined('ABSPATH') || !function_exists('add_filter')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

$fontAwesome_URL = esc_url( add_query_arg( 'ver', wp_styles()->registered['font-awesome']->ver, $wp_styles->registered['font-awesome']->src ) );

?>

<!DOCTYPE html>
<html <?php language_attributes() ?> class="no-js wf-inactive">
<head>

	<meta charset="<?php bloginfo( 'charset' ) ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ) ?>" />

	<?php
	wp_enqueue_script( 'lazysizes' );
	wp_enqueue_script( 'webfontloader' );

	wp_enqueue_style( 'Sa/copy/landing_1' );
	wp_enqueue_style( 'Sa/front/landing_1' );

	add_action( 'wp_print_styles', 'print_inline_landing_1_stylesheets' );
	function print_inline_landing_1_stylesheets() {
		global $fontAwesome_URL;
		$css = '';

		$bg_overlay = get_post_meta( get_the_ID(), '_bg_overlay', true );
		if ( false === $bg_overlay || empty( $bg_overlay ) || !is_array( $bg_overlay ) )
			$bg_overlay = array(
				'color' => 'black',
				'opacity' => 20,
			);

		$bg_color = $bg_overlay['color'];
		if ( has_post_thumbnail() ) {
			$bg_color = get_image_prominent_color( get_post_thumbnail_id() );
			if ( false === $bg_color )
				$bg_color = $bg_overlay['color'];
			$bg_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
		}

		if ( false !== ( $logo_image_id = get_more_post_thumbnail_id( get_the_ID(), 'logo' ) ) )
			$logo_image = wp_get_attachment_image_src( $logo_image_id, 'full' );

		if ( false !== ( $ul_bullet_image_id = get_more_post_thumbnail_id( get_the_ID(), 'ul-bullet' ) ) ) {
			define( 'LANDING_1_HAS_UL_BULLET_IMAGE', true );
			$ul_bullet_image = wp_get_attachment_image_src( $ul_bullet_image_id, 'full' );
		}

        foreach ( wp_styles()->queue as $handle ) {
            if ( 'header' !== wp_styles()->get_data( $handle, 'inline' ) )
                continue;

            $file = str_replace( get_stylesheet_directory_uri(), '', wp_styles()->registered[$handle]->src );

            if (
                file_exists( get_stylesheet_directory() . '/' . $file )
                && is_file( get_stylesheet_directory() . '/' . $file )
            ) {
                $file_contents = file_get_contents( get_stylesheet_directory() . '/' . $file );
                if ( '' !== $file_contents ) {
                    wp_styles()->done[] = $handle;
                    $css .= ( current_user_can( 'administrator' ) ? ' /** ' . $handle . ' **/ ' : '' ) . $file_contents;
                }
            }
        }

		if ( current_user_can( 'administrator' ) )
			$css .= ' /** inline styles **/ ';

		$css .= 'body { background-color: #' . $bg_color . ' }' .
			'#stage:after { background-color: ' . $bg_overlay['color'] . '; opacity: ' . ( $bg_overlay['opacity'] / 100 ) . '; }';
		if ( isset( $bg_image ) && is_array( $bg_image ) )
			$css .= 'html.js #stage.lazyloaded:before, html.no-js #stage:before { background-image: url(' . esc_url( $bg_image[0] ) . '); }';

		if ( isset( $ul_bullet_image ) && is_array( $ul_bullet_image ) )
			$css .= 'html.js #stage.lazyloaded main ul > li:before, html.no-js #stage main ul > li:before { ' .
				'width: ' . esc_attr( $ul_bullet_image[1] ) . 'px; ' .
				'height: ' . esc_attr( $ul_bullet_image[2] ) . 'px; ' .
				'background-image: url(' . esc_url( $ul_bullet_image[0] ) . ');' .
			'}';

        echo '<style id="inline-stylesheets" type="text/css">' . $css . '</style>';

		echo '<noscript>' .
			'<link rel="stylesheet" id="google-fonts-noscript" href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" />' .
			'<link rel="stylesheet" id="font-awesome-noscript" href="' . $fontAwesome_URL . '" />' .
		'</noscript>';
	}

	remove_action( 'wp_head',             'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles',     'print_emoji_styles' );
	remove_action( 'admin_print_styles',  'print_emoji_styles' );

	wp_head();
	?>

	<script type="text/javascript">
		WebFontConfig = {
			google: {
		      families: ['Roboto:400,400i,700']
		  	},
			custom: {
				families: ['FontAwesome'],
				urls: ['<?php echo $fontAwesome_URL ?>']
			},
		};
		if ('undefined' !== typeof WebFont)
			WebFont.load(WebFontConfig);

		window.lazySizesConfig = window.lazySizesConfig || {};
		document.documentElement.className = document.documentElement.className.replace('no-js','js');

		/*! modernizr 3.3.1 (Custom Build) | MIT *
		 * https://modernizr.com/download/?-cssvhunit-cssvwunit-flexbox-setclasses !*/
		!function(e,n,t){function r(e,n){return typeof e===n}function s(){var e,n,t,s,o,i,a;for(var l in C)if(C.hasOwnProperty(l)){if(e=[],n=C[l],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(s=r(n.fn,"function")?n.fn():n.fn,o=0;o<e.length;o++)i=e[o],a=i.split("."),1===a.length?Modernizr[a[0]]=s:(!Modernizr[a[0]]||Modernizr[a[0]]instanceof Boolean||(Modernizr[a[0]]=new Boolean(Modernizr[a[0]])),Modernizr[a[0]][a[1]]=s),g.push((s?"":"no-")+a.join("-"))}}function o(e){var n=S.className,t=Modernizr._config.classPrefix||"";if(x&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),x?S.className.baseVal=n:S.className=n)}function i(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):x?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function a(){var e=n.body;return e||(e=i(x?"svg":"body"),e.fake=!0),e}function l(e,t,r,s){var o,l,u,f,d="modernizr",p=i("div"),c=a();if(parseInt(r,10))for(;r--;)u=i("div"),u.id=s?s[r]:d+(r+1),p.appendChild(u);return o=i("style"),o.type="text/css",o.id="s"+d,(c.fake?c:p).appendChild(o),c.appendChild(p),o.styleSheet?o.styleSheet.cssText=e:o.appendChild(n.createTextNode(e)),p.id=d,c.fake&&(c.style.background="",c.style.overflow="hidden",f=S.style.overflow,S.style.overflow="hidden",S.appendChild(c)),l=t(p,e),c.fake?(c.parentNode.removeChild(c),S.style.overflow=f,S.offsetHeight):p.parentNode.removeChild(p),!!l}function u(e,n){return!!~(""+e).indexOf(n)}function f(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function d(e,n){return function(){return e.apply(n,arguments)}}function p(e,n,t){var s;for(var o in e)if(e[o]in n)return t===!1?e[o]:(s=n[e[o]],r(s,"function")?d(s,t||n):s);return!1}function c(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function m(n,r){var s=n.length;if("CSS"in e&&"supports"in e.CSS){for(;s--;)if(e.CSS.supports(c(n[s]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var o=[];s--;)o.push("("+c(n[s])+":"+r+")");return o=o.join(" or "),l("@supports ("+o+") { #modernizr { position: absolute; } }",function(e){return"absolute"==getComputedStyle(e,null).position})}return t}function h(e,n,s,o){function a(){d&&(delete T.style,delete T.modElem)}if(o=r(o,"undefined")?!1:o,!r(s,"undefined")){var l=m(e,s);if(!r(l,"undefined"))return l}for(var d,p,c,h,v,y=["modernizr","tspan","samp"];!T.style&&y.length;)d=!0,T.modElem=i(y.shift()),T.style=T.modElem.style;for(c=e.length,p=0;c>p;p++)if(h=e[p],v=T.style[h],u(h,"-")&&(h=f(h)),T.style[h]!==t){if(o||r(s,"undefined"))return a(),"pfx"==n?h:!0;try{T.style[h]=s}catch(g){}if(T.style[h]!=v)return a(),"pfx"==n?h:!0}return a(),!1}function v(e,n,t,s,o){var i=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+z.join(i+" ")+i).split(" ");return r(n,"string")||r(n,"undefined")?h(a,n,s,o):(a=(e+" "+E.join(i+" ")+i).split(" "),p(a,n,t))}function y(e,n,r){return v(e,t,t,n,r)}var g=[],C=[],w={_version:"3.3.1",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){C.push({name:e,fn:n,options:t})},addAsyncTest:function(e){C.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=w,Modernizr=new Modernizr;var S=n.documentElement,x="svg"===S.nodeName.toLowerCase(),_=w.testStyles=l;_("#modernizr { height: 50vh; }",function(n){var t=parseInt(e.innerHeight/2,10),r=parseInt((e.getComputedStyle?getComputedStyle(n,null):n.currentStyle).height,10);Modernizr.addTest("cssvhunit",r==t)}),_("#modernizr { width: 50vw; }",function(n){var t=parseInt(e.innerWidth/2,10),r=parseInt((e.getComputedStyle?getComputedStyle(n,null):n.currentStyle).width,10);Modernizr.addTest("cssvwunit",r==t)});var b="Moz O ms Webkit",z=w._config.usePrefixes?b.split(" "):[];w._cssomPrefixes=z;var E=w._config.usePrefixes?b.toLowerCase().split(" "):[];w._domPrefixes=E;var P={elem:i("modernizr")};Modernizr._q.push(function(){delete P.elem});var T={style:P.elem.style};Modernizr._q.unshift(function(){delete T.style}),w.testAllProps=v,w.testAllProps=y,Modernizr.addTest("flexbox",y("flexBasis","1px",!0)),s(),o(g),delete w.addTest,delete w.addAsyncTest;for(var N=0;N<Modernizr._q.length;N++)Modernizr._q[N]();e.Modernizr=Modernizr}(window,document);

	</script>

</head>

<body <?php body_class( ( array_key_exists( 'pixel-perfect', $_GET ) ? 'pixel-perfect' : '' ) . ( defined( 'LANDING_1_HAS_UL_BULLET_IMAGE' ) && LANDING_1_HAS_UL_BULLET_IMAGE ? ' has-ul-bullet-image' : '' ) ) ?>>

	<?php
	if ( have_posts() )
		while ( have_posts() ) {
			the_post();

			$image_logo_id = get_more_post_thumbnail_id( get_the_ID(), 'logo' );
			if ( false === $image_logo_id || empty( $image_logo_id ) ) {
				unset( $image_logo_id );
				$text_logo = get_post_meta( get_the_ID(), '_text_logo', true);
				if ( false === $text_logo || empty( $text_logo ) )
					unset( $text_logo );
			}
			?>

			<div id="stage" class="lazyload">

				<main id="post-<?php the_ID() ?>"<?php post_class() ?>>

					<?php
					if ( isset( $image_logo_id ) ) {
						$logo = new image_tag( array(
							'attachment_id' => $image_logo_id,
							'echo' => false,
						));
						echo '<h2>' . $logo->output() . '</h2>';
					} else if ( isset( $text_logo ) )
						echo '<h2>' . esc_html( $text_logo ) . '</h2>';
					?>

					<div class="entry">
						<?php the_content() ?>
					</div>

				</main>

			</div>

			<?php
		}

	wp_dequeue_script( 'wp-embed' );

	wp_footer();
	?>

</body>
</html>
