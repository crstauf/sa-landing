<?php

if (!defined('ABSPATH') || !function_exists('add_filter')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

class Sa_edit_landing_1_page {

    public static $bg_overlay_color = 'black';
    public static $bg_overlay_opacity = 20;

    static function hooks() {
        if (
			(
                (
                    array_key_exists( 'action', $_REQUEST )
                    && 'edit' === $_REQUEST['action']
                    && array_key_exists( 'post', $_REQUEST )
                    && 'template-landing_1.php' === get_post_meta( $_REQUEST['post'], '_wp_page_template', true )
                ) || (
                    array_key_exists( 'new_template', $_GET )
                    && 'landing_1' === $_GET['new_template']
                )
            )
        ) {
            add_filter( 'admin_body_class', array( __CLASS__, 'filter_admin_body_class' ) );
            add_action( 'add_meta_boxes', array( __CLASS__, 'action_add_meta_boxes' ) );
            add_filter( 'tiny_mce_before_init', array( __CLASS__, 'filter_tiny_mce_before_init' ) );
            add_filter( 'wp_calculate_image_sizes', array( __CLASS__, 'filter_wp_calculate_image_sizes' ), 10, 5 );
            add_action( 'admin_footer', array( __CLASS__, 'action_admin_footer' ) );

            add_post_thumbnail( 'page', 'logo', 'Logo' );
            add_post_thumbnail( 'page', 'ul-bullet', 'Unordered List Bullet' );
        }
    }

    static function filter_admin_body_class( $classes ) {
        if (
            array_key_exists( 'post', $_GET )
            && has_post_thumbnail( $_GET['post'] )
            && true == get_user_meta( get_current_user_id(), 'landing_1-background-overlay-preview' )
        )
            return $classes . ' background-overlay-preview';
    }

    static function action_add_meta_boxes() {
        add_meta_box( 'landing_1-options', 'Options', array( __CLASS__, 'meta_box' ), 'page', 'side' );
    }

        public static function meta_box($post) {
            wp_nonce_field( __FILE__, '_nonce-edit-landing_1-page' );

            $text_logo = '';

            $bg_overlay = get_post_meta( $post->ID, '_bg_overlay', true);
            $text_logo = get_post_meta( $post->ID, '_text_logo', true);

            if ( false !== $bg_overlay && !empty( $bg_overlay ) ) {
                self::$bg_overlay_color = $bg_overlay['color'];
                self::$bg_overlay_opacity = $bg_overlay['opacity'];
            }
            ?>

            <p><strong>Background Overlay</strong></p>
            <p class="howto">Select background image overlay color and opacity.</p>
            <p class="hide-if-no-js"><label><input type="checkbox" id="background-overlay-preview" name="background-overlay-preview" value="1"<?php checked( true, get_user_meta( get_current_user_id(), 'landing_1-background-overlay-preview', true ) ) ?> /> Live Preview</label></p>

            <label class="screen-reader-text" for="background-overlay-color">Background Overlay Color</label>
            <select name="_bg_overlay[color]" id="background-overlay-color">
                <option value="black"<?php selected( 'black', self::$bg_overlay_color ) ?>>Black</option>
                <option value="white"<?php selected( 'white', self::$bg_overlay_color ) ?>>White</option>
            </select>

            <label class="screen-reader-text" for="background-overlay-opacity">Background Overlay Opacity</label>
            <input type="number" name="_bg_overlay[opacity]" id="background-overlay-opacity" min="0" max="100" step="1" value="<?php echo esc_attr( self::$bg_overlay_opacity ) ?>" />%
            <input type="range" id="background-overlay-opacity-slide" min="0" max="100" value="<?php echo esc_attr( self::$bg_overlay_opacity ) ?>" style="width: 100%; vertical-align: middle;" />

            <p><strong>Title/Text Logo</strong></p>
            <p class="howto">Enter text box heading text (if logo is provided, image will override).</p>
            <input type="text" name="_text_logo" value="<?php echo esc_attr( $text_logo ) ?>" style="width: 100%;" />

            <?php
        }

    static function filter_tiny_mce_before_init( $init ) {
        global $wp_styles;

        $init['style_formats'][] =
            array( 'title' => 'Landing 1', 'items' => array(
                array( 'title' => 'Text Color', 'items' => array(
                    array( 'title' => 'Gray',          'selector' => '*', 'inline' => 'span', 'classes' => 'color-gray' ),
                    array( 'title' => 'Green',         'selector' => '*', 'inline' => 'span', 'classes' => 'color-green' ),
                ) ),
                array( 'title' => 'Background Color', 'items' => array(
                    array( 'title' => 'Gray',          'selector' => '*', 'inline' => 'span', 'classes' => 'bg-gray' ),
                    array( 'title' => 'Green',         'selector' => '*', 'inline' => 'span', 'classes' => 'bg-green' ),
                ) ),
                array( 'title' => 'Buttons', 'items' => array(
                    array( 'title' => 'Hover Effects', 'items' => array(
                        array( 'title' => 'Blue Background', 'selector' => '.btn', 'classes' => 'hover-bg-blue'),
                    ) ),
                    array( 'title' => 'Green w/ Icon', 'selector' => '*', 'inline' => 'a', 'classes' => 'btn bg-green' ),
                ) ),
            ) );

        $init['content_css']['Sa/copy/landing_1'] = add_query_arg(
            'ver', $wp_styles->registered['Sa/copy/landing_1']->ver,
            $wp_styles->registered['Sa/copy/landing_1']->src
        );

        $init['content_css']['font-awesome'] = add_query_arg(
            'ver', $wp_styles->registered['font-awesome']->ver,
            $wp_styles->registered['font-awesome']->src
        );

        $init['content_css']['google-fonts'] = 'https://fonts.googleapis.com/css?family=Roboto:400,400i,700';

        $init['content_css']['editor-style'] = get_stylesheet_directory_uri() . '/css/editor-style.php';
        if (array_key_exists('post',$_GET) && $ul_bullet_image_id = get_more_post_thumbnail_id($_GET['post'],'ul-bullet')) {
            if (false !== $ul_bullet_image_id && !empty($ul_bullet_image_id)) {
                $ul_bullet_image = wp_get_attachment_image_src($ul_bullet_image_id,'full');
                if ( false !== $ul_bullet_image && is_array( $ul_bullet_image ) ) {
                    $init['body_class'] .= ' has-ul-bullet-image';
                    $init['content_css']['editor-style'] =
                        add_query_arg(
                            'ul-bullet[0]',
                            esc_url($ul_bullet_image[0]),
                        add_query_arg(
                            'ul-bullet[1]',
                            esc_html($ul_bullet_image[1]),
                        add_query_arg(
                            'ul-bullet[2]',
                            esc_html($ul_bullet_image[2]),
                        $init['content_css']['editor-style']
                    )));
                }
            }
        }

        return $init;
    }

    static function filter_admin_post_thumbnail_size( $size ) {
        if (
            array_key_exists( 'action', $_REQUEST )
            && 'get-post-thumbnail-html' === $_REQUEST['action']
            && array_key_exists( 'post_id', $_REQUEST )
            && 'template-landing_1.php' === get_post_meta( $_REQUEST['post_id'], '_wp_page_template', true )
        )
            $size = 'full';
        return $size;
    }

    static function filter_wp_calculate_image_sizes( $sizes, $size, $image_src, $image_meta, $attachment_id ) {
        if ( !array_key_exists( 'post', $_GET ) )
            return $sizes;
        if ( get_post_thumbnail_id( $_GET['post'] ) !== $attachment_id )
            return $sizes;

        return '100vw';
    }

    static function action_admin_footer() {
        ?>

        <style type="text/css">
            a#set-post-thumbnail,
            a#set-post-thumbnail img { width: 100% !important; }
            body.background-overlay-preview a#set-post-thumbnail { position: relative; }
            body.background-overlay-preview a#set-post-thumbnail:before {
                content: '';
                position: absolute;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                background-color: <?php echo esc_attr( self::$bg_overlay_color ) ?>;
                opacity: <?php echo esc_attr( self::$bg_overlay_opacity / 100 ) ?>;
            }
            body.background-overlay-preview a#set-post-thumbnail:hover:before { opacity: 0; }
        </style>
        <style id="jQuery-css-1" type="text/css"></style>
        <style id="jQuery-css-2" type="text/css"></style>

        <script type="text/javascript">
            jQuery("body").on('DOMSubtreeModified','#postimagediv .inside',function() {
                if (!jQuery(this).find('img').length)
                    jQuery("body").removeClass('background-overlay-preview');
                else if (jQuery("#background-overlay-preview").is(':checked'))
                    jQuery("body").addClass('background-overlay-preview');
            });
            jQuery("#background-overlay-preview").on('change',function() {
                if (jQuery("a#set-post-thumbnail").find('img').length && jQuery(this).is(':checked'))
                    jQuery("body").addClass('background-overlay-preview');
                else
                    jQuery("body").removeClass('background-overlay-preview');
            })
            jQuery("#background-overlay-opacity-slide,#background-overlay-opacity").on('input change',function() {
                var val = jQuery(this).val();
                jQuery("#background-overlay-opacity-slide,#background-overlay-opacity").val(val);
                jQuery("style#jQuery-css-1").html('body.background-overlay-preview a#set-post-thumbnail:before{ opacity:' + (val/100) + '; }');
            });
            jQuery("#background-overlay-color").on('change',function() {
                var val = jQuery(this).val();
                jQuery("style#jQuery-css-2").html('body.background-overlay-preview a#set-post-thumbnail:before{ background-color:' + val + '; }');
            });
        </script>

        <?php
    }

    static function action_save_post( $post_id ) {
        if ( !isset( $_POST ) ) return $post_id;
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) return $post_id;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;
        if ( !current_user_can( 'edit_page', $post_id ) ) return $post_id;
        if ( !array_key_exists( '_nonce-edit-landing_1-page', $_POST ) || !wp_verify_nonce( $_POST['_nonce-edit-landing_1-page'], __FILE__ ) ) return $post_id;

        update_post_meta( $post_id, '_bg_overlay', $_POST['_bg_overlay'] );
        update_post_meta( $post_id, '_text_logo', $_POST['_text_logo'] );

        if ( array_key_exists( 'background-overlay-preview', $_POST ) && 1 == $_POST['background-overlay-preview'] )
            update_user_meta( get_current_user_id(), 'landing_1-background-overlay-preview', true );
        else
            delete_user_meta( get_current_user_id(), 'landing_1-background-overlay-preview' );
    }

}

?>
