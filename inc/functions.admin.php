<?php

if (!defined('ABSPATH') || !function_exists('add_filter')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

require_once 'class.Sa-edit-landing_1-page.php';

class SA_ActionsAdmin {

    static function init() {
        wp_register_script( 'more-post-thumbnails', get_theme_asset_uri( 'js/more-post-thumbnails.min.js' ), array( 'jquery' ),'0.0.2' );
    	wp_register_script( 'mpt-mediamodal',       get_theme_asset_uri( 'js/mpt_mediamodal.min.js' ), array( 'jquery', 'more-post-thumbnails' ), '0.0.2' );
    }

}

class SA_FiltersAdmin {

    static function mce_buttons_2( $buttons ) {
    	array_unshift( $buttons, 'styleselect' );
    	return $buttons;
    }

    static function tiny_mce_before_init( $init ) {
        if (
            array_key_exists( 'style_formats', $init )
            && is_string( $init['style_formats'] )
        )
            $init['style_formats'] = ( array ) json_decode( $init['style_formats'] );
        else
            $init['style_formats'] = array();

    	if (
            array_key_exists( 'content_css', $init )
            && is_string( $init['content_css'] )
        )
    		$init['content_css'] = ( array ) json_decode( $init['content_css'] );
        else
            $init['content_css'] = array();

    	$init['preview_styles'] .= ' color background-color';

    	return $init;
    }

    static function tiny_mce_before_init_9999999($init) {
        if (
            array_key_exists( 'style_formats', $init )
            && is_array( $init['style_formats'] )
        ) {
        	$temp = $init['style_formats'];
        	$values = array_values( $temp );
        	$init['style_formats'] = json_encode( $values );
        }

    	if (
            array_key_exists( 'content_css', $init )
            && is_array( $init['content_css'] )
        ) {
        	$temp = $init['content_css'];
        	$values = array_values( $temp );
        	$init['content_css'] = json_encode( $values );
        }
    	return $init;
    }

}

?>
