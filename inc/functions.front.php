<?php

if (!defined('ABSPATH') || !function_exists('add_filter')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

require_once 'class.enhance-wp-enqueues.php';

class SA_ActionsFront {

    static function init() {
        wp_register_script( 'lazysizes',          get_theme_asset_uri( 'js/lazysizes.min.js' ), array(), '2.0.0-init' );
        wp_register_script( 'webfontloader',      get_stylesheet_directory_uri() . '/js/webfontloader.js', array(), '1.6.26-init' );

        wp_register_style( 'Sa/front/stylesheet', get_theme_asset_uri( 'style.min.css' ), array(), 'init' );
        wp_register_style( 'Sa/front/landing_1',  get_theme_asset_uri( 'css/landing-1.min.css' ), array( 'Sa/copy/landing_1' ), 'init' );

        wp_styles()->registered['Sa/front/landing_1']->extra['inline'] = 'header';

        wp_scripts()->registered['lazysizes']->extra['async'] = true;
        wp_scripts()->registered['webfontloader']->extra['async'] = true;
    }

    static function wp_print_styles() {

        $css = '';

        foreach ( array(
            'bc/copy',
            'bc/above-fold',
            'bc/above-fold/home',
        ) as $handle ) {
            if ( !wp_style_is( $handle ) )
                continue;

            $file = basename( wp_styles()->registered[$handle]->src );

            if (
                file_exists( get_stylesheet_directory() . '/css/' . $file )
                && is_file( get_stylesheet_directory() . '/css/' . $file )
            ) {
                $file_contents = file_get_contents( get_stylesheet_directory() . '/css/' . $file );
                if ( '' !== $file_contents ) {
                    wp_styles()->done[] = $handle;
                    $css .= ( current_user_can( 'administrator' ) ? '/* ' . $handle . ' */ ' : '' ) . $file_contents;
                }
            }
        }

        if ( '' !== $css )
            echo '<style type="text/css">' . htmlentities( $css ) . '</style>';


    }

}
