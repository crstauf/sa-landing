<?php
/*
Plugin Name: More Post Thumbnails
Description: Adds the ability to add more post thumbnails to a post type.
Version: 0.0.1
Author: Caleb Stauffer
Author URI: http://develop.calebstauffer.com
*/

if (!defined('ABSPATH') || !function_exists('add_filter')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

if (is_admin())
	new more_post_thumbnails;

class more_post_thumbnails {

	public static $thumbs = array();

	function __construct() {
		add_action('after_setup_theme',array(__CLASS__,'admin'));
	}

	public static function addnew($post_types,$id,$label = '') {
		if (!is_array($post_types)) $post_types = array($post_types);
		if ('' == $label) $label = ucwords(str_replace('-',' ',str_replace('_',' ',$id)));
		foreach ($post_types as $post_type) {
			self::$thumbs[$post_type][$id] = $label;
		}
	}

	public static function admin() {
		add_action('admin_head-post.php',array(__CLASS__,'enqueue_scripts'));
		add_action('wp_ajax_set-post-more-thumbnail',array(__CLASS__,'ajax_set'));
		add_action('wp_ajax_remove-post-more-thumbnail',array(__CLASS__,'ajax_remove'));
		foreach (array_keys(self::$thumbs) as $post_type) {
			add_action('add_meta_boxes_' . $post_type,array(__CLASS__,'add_meta_boxes'));
		}
	}

		public static function enqueue_scripts() {
			if (array_key_exists('post_type',$_GET)) $post_type = $_GET['post_type'];
			else if (array_key_exists('post',$_GET)) $post_type = get_post_type($_GET['post']);

			if (!array_key_exists($post_type,self::$thumbs)) return;

			wp_enqueue_script('more-post-thumbnails');
			wp_enqueue_script('mpt-mediamodal');
		}

		public static function screen() {
			$screen = get_current_screen();
			if ('post' == $screen->base && array_key_exists($screen->post_type,self::$thumbs)) {
				wp_enqueue_script('more-post-thumbnails');
				wp_enqueue_script('mpt-mediamodal');
			}
		}

		public static function add_meta_boxes($post) {
			$post_type = get_post_type($post);
			foreach (self::$thumbs[$post_type] as $more_id => $label)
				add_meta_box('mpt-' . $post_type . '-' . $more_id,$label,array(__CLASS__,'metabox'),$post_type,'side','default',array('more_id' => $more_id));
		}

			public static function metabox($post,$args) {
				$post_type = get_post_type($post);
				$more_id = $args['args']['more_id'];
				if ($img_id = get_post_meta($post->ID,'_thumbnail_' . $more_id . '_id',true)) {
					$img = wp_get_attachment_image_src($img_id,'medium');
					echo '<p class="hide-if-no-js"><a href="#" id="set-post-' . esc_attr($more_id) . '-thumbnail"><img src="' . esc_attr($img[0]) . '" width="' . esc_attr($img[1]) . '" height="' . esc_attr($img[2]) . '" style="width: 100%; max-width: 300px; height: auto;" /></a></p>';
					echo '<p class="hide-if-no-js"><a href="#" id="remove-' . esc_attr($post_type . '-' . $more_id) . '-thumbnail" class="remove-post-more-thumbnail" data-more-thumbnail-id="' . esc_attr($more_id) . '" data-more-label="' . esc_attr(self::$thumbs[$post_type][$more_id]) . '">Remove ' . esc_html(self::$thumbs[$post_type][$more_id]) . '</a></p>';
				} else
					echo '<p class="hide-if-no-js"><a href="#" id="set-' . esc_attr($post_type . '-' . $more_id) . '-thumbnail" class="set-post-more-thumbnail" data-more-thumbnail-id="' . esc_attr($more_id) . '" data-more-label="' . esc_attr(self::$thumbs[$post_type][$more_id]) . '">Set ' . esc_html(self::$thumbs[$post_type][$more_id]) . '</a></p>';
			}

		public static function ajax_set() {
			$post_id = $_REQUEST['post_id'];
			$thumb_id = $_REQUEST['thumbnail_id'];
			$more_id = $_REQUEST['more_id'];
			$label = $_REQUEST['more_label'];

			do_action('set-more-post-thumbnail',$thumb_id,false,$post_id,$more_id);

			if ($meta = update_post_meta($post_id,'_thumbnail_' . $more_id . '_id',$thumb_id)) {
				$thumb = wp_get_attachment_image_src($thumb_id,'medium',true);
				echo json_encode('src="' . $thumb[0] . '" width="' . $thumb[1] . '" height="' . $thumb[2] . '" alt="' . get_the_title($thumb_id) . '"');
			} else echo json_encode('Unable to set ' . $more_id . ' image');

			die();
		}

		public static function ajax_remove() {
			echo json_encode(print_r($_REQUEST,true));
			$metakey = '_thumbnail_' . $_REQUEST['more_id'] . '_id';
			delete_post_meta($_REQUEST['post_id'],$metakey);
			die();
		}

}

function add_post_thumbnail($post_types,$id,$label = '') {
	if (!is_admin() || !class_exists('more_post_thumbnails'))
		return;
	more_post_thumbnails::addnew($post_types,$id,$label);
}

function get_more_post_thumbnail_id($post_id,$more_id) {
	return get_post_meta($post_id,'_thumbnail_' . $more_id . '_id',true);
}

?>
