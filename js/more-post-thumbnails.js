jQuery(function($) {

	var mod = new MPT_mediamodal({
		calling_selector: 'a.set-post-more-thumbnail',
		cb : function(attachment) {
			$.post(ajaxurl,{
				action:			'set-post-more-thumbnail',
				thumbnail_id:	attachment.id,
				post_id:		$('#post_ID').val(),
				more_id: 		mod.btn[0].dataset.moreThumbnailId,
				more_label:		mod.btn[0].dataset.moreLabel,
			},function(returned) {
				var data = $.parseJSON(returned);
				$("#" + mod.btn[0].id).closest('div.inside').html('<p class="hide-if-no-js"><a href="#" id="set-post-' + mod.btn[0].dataset.moreThumbnailId + '-thumbnail"><img ' + data + ' style="width: 100%; max-width: 300px; height: auto;" /></a></p><p class="hide-if-no-js"><a href="#" id="remove-' + $("#post_type").val() + '-' + mod.btn[0].dataset.moreThumbnailId + '-thumbnail" class="remove-post-more-thumbnail" data-more-thumbnail-id="' + mod.btn[0].dataset.moreThumbnailId + '" data-more-label="' + mod.btn[0].dataset.moreLabel + '">Remove ' + mod.btn[0].dataset.moreLabel + ' image</a></p>');
			});
		},
	});

	$(document).on('click','a.remove-post-more-thumbnail',function(ev) {
		ev.preventDefault();
		var that = $(this);
		$.post(ajaxurl,{
			action:		'remove-post-more-thumbnail',
			post_id: 	$('#post_ID').val(),
			more_id:	that.attr('data-more-thumbnail-id'),
			more_label:	that.attr('data-more-label'),
		},function(returned) {
			var data = $.parseJSON(returned);
			that.closest('div.inside').html('<p class="hide-if-no-js"><a href="#" id="set-' + $("#post_type").val() + '-' + that.attr('data-more-thumbnail-id') + '-thumbnail" class="set-post-more-thumbnail" data-more-thumbnail-id="' + that.attr('data-more-thumbnail-id') + '" data-more-label="' + that.attr('data-more-label') + '">Set ' + that.attr('data-more-label') + ' image</a></p>');
		});
	});

});
