<?php

if (!defined('ABSPATH') || !function_exists('add_filter')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

add_theme_support( 'post-thumbnails' );

require_once 'inc/class.image-tag.php';
require_once 'inc/class.more-post-thumbnails.php';

require_once 'inc/functions.' . ( is_admin() ? 'admin' : 'front' ) . '.php';

add_action( 'init', array( 'SA_Actions', 'init' ) );

if ( is_admin() ) {

    add_action( 'after_setup_theme',    array( 'Sa_edit_landing_1_page', 'hooks' ), 9 );
	add_action( 'init',                 array( 'SA_ActionsAdmin', 'init' ) );
    add_filter( 'mce_buttons_2',        array( 'SA_FiltersAdmin', 'mce_buttons_2' ) );
    add_filter( 'tiny_mce_before_init', array( 'SA_FiltersAdmin', 'tiny_mce_before_init' ) );
    add_filter( 'tiny_mce_before_init', array( 'SA_FiltersAdmin', 'tiny_mce_before_init_9999999' ), 9999999 );
    add_action( 'save_post',            array( 'Sa_edit_landing_1_page', 'action_save_post' ) );

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		add_filter( 'admin_post_thumbnail_size', array( 'Sa_edit_landing_1_page', 'filter_admin_post_thumbnail_size' ) );
	}

} else {

    add_action( 'init',                 array( 'SA_ActionsFront', 'init' ) );
    add_filter( 'script_loader_tag',    array('enhance_wp_enqueues','tag'), 10, 2);
	add_filter( 'style_loader_tag',     array('enhance_wp_enqueues','tag'), 10, 2);
	add_action( 'wp_head',              array('enhance_wp_enqueues','tags'), 8);
	add_action( 'wp_head',              array('enhance_wp_enqueues','tags'), 9);
    add_action( 'wp_footer',            array('enhance_wp_enqueues','tags'), 8);
	add_action( 'wp_footer',            array('enhance_wp_enqueues','tags'), 9);

}

class SA_Actions {

    static function init() {
        global $wp_styles;
        // register assets used in both front and back ends
        wp_register_style( 'font-awesome',      get_theme_asset_uri( 'css/font-awesome.min.css' ), array(), '4.6.3' );
        wp_register_style( 'Sa/copy/landing_1', get_theme_asset_uri( 'css/copy-landing_1.min.css' ), array(), 'init' );

        $wp_styles->registered['Sa/copy/landing_1']->extra['inline'] = 'header';
    }

}

function get_theme_asset_uri( $theme_path ) {
	// if SCRIPT_DEBUG is true, and file exists, serve un-minified assets
    if (
        ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG )
		|| !file_exists( trailingslashit( get_stylesheet_directory() ) . $theme_path )
    ) {
        $unminified_theme_path = str_replace( '.min', '', $theme_path );
        if ( file_exists( trailingslashit( get_stylesheet_directory() ) . $unminified_theme_path ) )
            $theme_path = $unminified_theme_path;
    }

    return trailingslashit( get_stylesheet_directory_uri() ) . $theme_path;
}

function get_image_prominent_color( $attachment_id, $image_path = false ) {
	if ( false !== $attachment_id ) { // to support non-WordPress uploaded images
		$image = wp_get_attachment_image_src( $attachment_id, 'medium' );
		$filename = basename( $image[0] );
		$image_path = dirname( get_attached_file( $attachment_id ) ) . '/' . $filename;
	}

	if ( !file_exists( $image_path ) )
        return false;

	require_once __DIR__ . '/inc/class.Sa-get-most-common-colors.php';
	$class = new GetMostCommonColors();
	$colors = array_keys( $class->Get_Color( $image_path, 1 ) );
	return $colors[0];
}

?>
